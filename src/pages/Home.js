import '../css/Home.css';
import Carousel from 'react-bootstrap/Carousel';


function Home() {
  return (
    <div>
      <header className="Home-header">
        <div className="logo">
        mov'interact
        </div>        
      </header>


      <body className="Home-body">
        <div className="bandeau">
          
          <div className="text1">
          FILM
        </div>
        <div className="text2">
          Regarder le film interactif maintenant
         
        </div>
        <div className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
             aliquip ex ea commodo consequat

          </div>
          <div className="btn-grad">
            Lancer&nbsp;la&nbsp;lecture
            </div>
        </div>
        <div className="image">
        <Carousel className="Carousel" pause='hover' interval='5000' slide='TRUE' >
  <Carousel.Item className="items">
    <img id="im1"
      className="d-inline w-auto"
      src={require('../assets/it.jpg')}
      alt="First slide"
    />
    <Carousel.Caption className="caption">
      <h3>First slide label</h3>
      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item className="items">
    <img id="im3"
      className="d-inline w-auto"
      src={require('../assets/sw.jpg')}
      alt="Second slide"
    />

    <Carousel.Caption className="caption">
      <h3>Second slide label</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item className="items">
    <img id="im2"
      className="d-inline w-auto"
      src={require('../assets/dune-image.jpg')}
      alt="Third slide"
    />

    <Carousel.Caption className="caption">
      <h3>Third slide label</h3>
      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
        </div>
      </body>
      
      </div>
  );
}

export default Home;
